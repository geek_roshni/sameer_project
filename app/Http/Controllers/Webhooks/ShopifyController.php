<?php

namespace App\Http\Controllers\Webhooks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Shopify\ProductAPI;
use App\Services\DanJewellers\OrderAPI;
use App\Models\Product;

class ShopifyController extends Controller
{
   
    public function __construct()
    {
        $this->danJjewellersapi = OrderAPI::init();
    }

    /*==== When new order placed in shopify.Send supplier =====*/
    public function handleNewOrder(Request $request)
    {       
        $json = (array) $request->json()->all();

        $order_number = $json['order_number'];
        $first_name = $json['billing_address']['first_name'];
        $last_name = $json['billing_address']['last_name'];
        $customer_name = $first_name." ".$last_name;

        $address1 = $json['shipping_address']['address1'];
        $city = $json['shipping_address']['city'];
        $zip = $json['shipping_address']['zip'];
        $country = $json['shipping_address']['country'];
        $delivery_address = $address1.", ".$city.", ".$country." ".$zip;

        $line_items = $json['line_items'];
        for ($i=0; $i < count($line_items); $i++) { 
            $shopify_product_id = $line_items[$i]['product_id'];

            $product = Product::where('shopify_product_id',$shopify_product_id)->get();
            $product_arr = $product->toArray();

            if(!empty($product_arr)){
                $suppiler_prduct_id  = $product_arr[0]['product_id'];
                $quantity = $line_items[$i]['quantity'];

                $order = array('OrderXml' => '<Order>
                                            <OrderHeader>
                                            <PurchaseOrderNumber>"'.$order_number.'"</PurchaseOrderNumber>
                                            <Notes>Placed via API</Notes>
                                            <CustomerName>"'.$customer_name.'"</CustomerName>
                                            <DeliveryAddress>"'.$delivery_address.'"</DeliveryAddress>
                                            </OrderHeader>
                                            <OrderLines>
                                            <OrderLine ProductID="'.$suppiler_prduct_id.'" Quantity="'.$quantity.'"/>
                                            </OrderLines>
                                        </Order>'
                            ); 
                $rs = $this->danJjewellersapi->createOrder($order);
                echo "Created Order Id:";
                print_r($rs);
                $supplier_data = $rs."<br>";
                $filepath = public_path().'/supplier_new_oder.txt';
                $file1 = fopen($filepath,"a");
                fwrite($file1,print_r($supplier_data ,true));
                fclose($file1);
            } 

        }
 
        $filepath = public_path().'/new_oder.txt';
        $file = fopen($filepath,"a");
        fwrite($file,print_r($json ,true));
        fclose($file);


    }
}
