<?php
 
namespace App\Http\Controllers\Api;

use GuzzleHttp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product; 
use App\Models\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Services\Shopify\ProductAPI;

class ImportProductController extends Controller
{  
  public function __construct(){
        $this->middleware('auth');
        $this->shopifyapi = ProductAPI::init();
    }
 
    //Display from
    public function index(){
    // phpinfo(); 
      return view('shopifyProduct.import'); 
    }
    //import product in shopify as well database
    public function importProduct(Request $request){
      $request->validate([
            'file' => 'required'
        ], [
            'file.required' => 'Please select product csv file'
        ]);
       $file = $request->file('file');
        if($file){
          $extension = $file->getClientOriginalExtension();
          $fileName  = $file->getFilename().'.'.$extension;
          $success = Storage::disk('public')->put($fileName ,  File::get($file));
          $filePath = public_path($fileName);

          if($filePath){
            $csvData=array_map('str_getcsv',file($file));
            $csvHeader = $csvData[0];
            unset($csvData[0]);
            /*check header name in csv start*/
            if(trim($csvHeader[0]) == 'ProductID' && trim($csvHeader[1]) == 'DansSKU' && trim($csvHeader[2]) == 'ProductTitle' && trim($csvHeader[3]) == 'ProductDescription' && trim($csvHeader[4]) == 'ParentProductID' && trim($csvHeader[5]) == 'Variants' && trim($csvHeader[6]) == 'Weight' && trim($csvHeader[7]) == 'Height/Length' && trim($csvHeader[8]) == 'Width' && trim($csvHeader[9]) == 'Depth' && trim($csvHeader[10]) == 'Condition' && trim($csvHeader[11]) == 'Hallmark' && trim($csvHeader[12]) == 'Collections' && trim($csvHeader[13]) == 'Tag1' && trim($csvHeader[14]) == 'Tag2' && trim($csvHeader[15]) == 'Tag3' && trim($csvHeader[16]) == 'Stock' && trim($csvHeader[17]) == 'SellPrice'){
            
            $parentProducts = array();
            $childProducts = array();
            $variantProduct = array();
            $variantArray = array();

            /*===== csv file convert into array start ======*/
            foreach($csvData as $row){
              $product = array_combine($csvHeader, $row);
              if(empty(trim($product['ParentProductID'])) || $product['ParentProductID'] == 'NULL'){
                  $parentProducts[] = $product;
              }
              if(!empty(trim($product['ParentProductID'])) && trim($product['ParentProductID']) > 0){
                  $childProducts[trim($product['ParentProductID'])][] = $product;
              }  
            }
           /*==== csv file convert into array start  =========*/

            /*===== parent and child array combine in one array start ======*/
            foreach ($childProducts as $key => $value) {
              $variantProduct['ProductID'] = $value[0]['ProductID'];
              $variantProduct['ParentProductID'] = $value[0]['ParentProductID'];
              $variantProduct['ProductTitle'] = $value[0]['ProductTitle'];
              $variantProduct['ProductDescription'] = $value[0]['ProductDescription'];
              $variantProduct['csvVariants'] = $value[0]['Variants'];
              $variantProduct['Weight'] = $value[0]['Weight'];
              $variantProduct['Height/Length'] = $value[0]['Height/Length'];
              $variantProduct['Width'] = $value[0]['Width'];
              $variantProduct['Depth'] = $value[0]['Depth'];
              $variantProduct['SellPrice'] = $value[0]['SellPrice'];
              $variantProduct['Collections'] = $value[0]['Collections'];
              $variantProduct['Tag1'] = $value[0]['Tag1'];
              $variantProduct['Tag2'] = $value[0]['Tag2'];
              $variantProduct['Tag3'] = $value[0]['Tag3'];
              $variantProduct['Stock'] = $value[0]['Stock'];
              $variantProduct['Condition'] = $value[0]['Condition'];
              $variantProduct['Hallmark'] = $value[0]['Hallmark'];
              $variantProduct[0] = $value;
              array_push($variantArray,  $variantProduct);
            }
            $parentProducts = array_merge($parentProducts,$variantArray);
             /*======= parent and child array combine in one array end ======*/
            
            for ($i=0; $i < count($parentProducts); $i++) {
              $productPayload = array();

              $productPayload['id'] = $parentProducts[$i]['ProductID'];
              $product_id = $productPayload['id'];
              $product = Product::where('product_id',$product_id)->get();
              $product_arr = $product->toArray();
               /*====== Product update code start =======*/
              if(!empty($product_arr)){
                $collection = $parentProducts[$i]['Collections'];
                if(($collection !='Rings') && ($collection != 'Chains')){
                if(isset($parentProducts[$i][0])){
                  /*====== product variant loop start ========*/
                  for ($j=0; $j <= count($parentProducts[$i][0])-1; $j++) {

                    $product_id = $parentProducts[$i][0][$j]['ProductID'];

                    $variantProduct = Product::where('product_id',$product_id)->get();
                    $product_variant_arr = $variantProduct->toArray();
                    
                    if(!empty($product_variant_arr)){
                      $product_variant_id  = $product_variant_arr[0]['shopify_product_variant_id'];
                      $option1  = $parentProducts[$i][0][$j]['Weight'];
                      $option2 = $parentProducts[$i][0][$j]['Height/Length'];
                      $option3 = $parentProducts[$i][0][$j]['Width'];
                      if(empty($option1)){ $option1 = 0; }
                      if(empty($option2)){ $option2 = 0; }
                      if(empty($option3)){ $option3 = 0; }
                      /*call update shopify api endpoint function*/
                      sleep(2);
                      $this->updateShopifyProductVariant($product_variant_id,$option1,$option2,$option3);
                    }
                  }
                  /*product variant loop end*/
                  
                }
                /*======= parent option update code start ======*/
                 else{
                  $product_variant_id  = $product_arr[0]['shopify_product_variant_id'];
                  $option1 = $parentProducts[$i]['Weight'];
                  $option2 = $parentProducts[$i]['Height/Length'];
                  $option3 = $parentProducts[$i]['Width'];
                  if(empty($option1)){ $option1 = 0; }
                  if(empty($option2)){ $option2 = 0; }
                  if(empty($option3)){ $option3 = 0; }
                  /*call update shopify api endpoint function*/
                  sleep(2);
                  $this->updateShopifyProductVariant($product_variant_id,$option1,$option2,$option3);
                }
              }elseif (($collection =='Rings') || ($collection == 'Chains')) {

                if(isset($parentProducts[$i][0])){
                  /*====== product variant loop start ========*/
                  for ($j=0; $j <= count($parentProducts[$i][0])-1; $j++) {

                    $product_id = $parentProducts[$i][0][$j]['ProductID'];

                    $variantProduct = Product::where('product_id',$product_id)->get();
                    $product_variant_arr = $variantProduct->toArray();
                    
                    if(!empty($product_variant_arr)){

                      $product_variant_id  = $product_variant_arr[0]['shopify_product_variant_id'];
                      $option1  = $parentProducts[$i][0][$j]['Weight'];
                      $option2 = $parentProducts[$i][0][$j]['Variants'];
                      if(empty($option1)){ $option1 = 0; }
                      if(empty($option2)){ $option2 = 0; }
                      /*call update shopify api endpoint function*/
                        sleep(2);
                      $this->updateShopifyRingProductVariant($product_variant_id,$option1,$option2);
                    }
                  }
                  
                }
                /*======= parent option update code start ======*/
                 else{
                  $product_variant_id  = $product_arr[0]['shopify_product_variant_id'];

                  $option1 = $parentProducts[$i]['Weight'];
                  $option2 = $parentProducts[$i]['Variants'];

                  if(empty($option1)){ $option1 = 0; }
                  if(empty($option2)){ $option2 = 0; }
                  sleep(2);
                  /*call update shopify api endpoint function*/
                  $this->updateShopifyRingProductVariant($product_variant_id,$option1,$option2);
                }
              
              }
                /*======= parent option update code end ========*/

                   /*====== Product update code end =======*/
                  /* ================================================*/

                  /*====== Product add code start =======*/
              }else{
                if($parentProducts[$i]['Hallmark'] && $parentProducts[$i]['Condition']){
                  $description = '<p>'.$parentProducts[$i]['ProductDescription'].'</p><p><b>Hallmark: </b>'.$parentProducts[$i]['HallomarkExempt'].'</p><p><b>Condition: </b>'.$parentProducts[$i]['Condition'].'</p>';
                }
                elseif($parentProducts[$i]['Hallmark']){
                  $description = '<p>'.$parentProducts[$i]['ProductDescription'].'</p><p><b>Hallmark: </b>'.$parentProducts[$i]['HallomarkExempt'].'</p>';
                }
                elseif($parentProducts[$i]['Condition']){
                 $description = '<p>'.$parentProducts[$i]['ProductDescription'].'</p><p><b>Condition: </b>'.$parentProducts[$i]['Condition'].'</p>';
                }else{
                    $description = $parentProducts[$i]['ProductDescription'];
                }
                 $productPayload['title'] = addslashes($parentProducts[$i]['ProductTitle']);
                  $productPayload['body_html'] = $description;
                  $productPayload['tags'] = $parentProducts[$i]['Tag1'].','.$parentProducts[$i]['Tag2'].','.$parentProducts[$i]['Tag3'];
                  $productPayload['product_type'] = $parentProducts[$i]['Collections'];
                  /*if($productPayload['body_html']){
                     $productPayload['body_html'] = preg_replace('/[^a-zA-Z0-9_ -]/s', '',$parentProducts[$i]['ProductTitle']);
                  }*/
                   if($parentProducts[$i]['ParentProductID']) {
                      $images = $this->imageUpload($parentProducts[$i]['ParentProductID']);
                    }else{
                      $images =$this->imageUpload($parentProducts[$i]['ProductID']);  
                    }
                    if($images){
                      $productPayload['images'] = $images;
                    }

                $collection = trim($parentProducts[$i]['Collections']);
                /*============== Collection is Rings ================*/
                if ($collection == 'Rings' || $collection == 'Chains') {
                  $productPayload['options'] = array(array("name"=> "Weight","position"=>1),
                                array("name"=> "Size","position"=>2));
                  $productPayload['variants'] = array();

                  if(isset($parentProducts[$i][0])){
                  foreach ($parentProducts[$i][0] as $key => $value) {
                    $option1  = $value['Weight'];
                    $option2  = $value['Variants'];
                    if(empty($option1)){ $option1 = 0; }
                    if(empty($option2)){ $option2 = 0; }
                    $data = array( 
                        "id" =>$value['ProductID'],
                        "parent_id" => $value['ProductID'],
                        "option1" =>$option1,
                        "option2" =>$option2,
                        "inventory_management"=>"shopify",
                        "price" =>$value['SellPrice'], 
                        "inventory_quantity" => $value['Stock'],
                       "presentment_prices"=>array("price"=>array("currency_code"=>"GBP","amount"=>$value['SellPrice']))
                        );
                        array_push($productPayload['variants'],$data);
                  }
                }else{
                  $option1  = $parentProducts[$i]['Weight'];
                  $option2 = $parentProducts[$i]['Variants'];
                  if(empty($option1)){ $option1 = 0; } 
                  if(empty($option2)){ $option2 = 0; }
                  $productPayload['variants'][] = array( 

                  "id" =>$parentProducts[$i]['ProductID'],
                  "parent_id" => $parentProducts[$i]['ProductID'],
                  "option1" =>$option1,
                  "option2" =>$option2,
                  "inventory_management"=>"shopify",
                 // "position" => "1", 
                  "price" =>$parentProducts[$i]['SellPrice'],
                  "inventory_quantity" => $parentProducts[$i]['Stock'],
                  "presentment_prices"=>array("price"=>array("currency_code"=>"GBP","amount"=>$parentProducts[$i]['SellPrice']))
                   );
                }
                
                $this->productAddShopify($productPayload,$parentProducts[$i]['ProductID']);
                sleep(2);

              }else{ //Product are not chain and ring
                $productPayload['options'] = array(array("name"=> "Weight","position"=>1),
                              array("name"=> "Height","position"=>2),
                              array("name"=> "Width","position"=>3));
                $productPayload['variants'] = array();
                
                if(isset($parentProducts[$i][0])){
                foreach ($parentProducts[$i][0] as $key => $value) {
                  $option1  = $value['Weight'];
                  $option2 = $value['Height/Length'];
                  $option3 = $value['Width'];
                  if(empty($option1)){ $option1 = 0; }
                  if(empty($option2)){ $option2 = 0; }
                  if(empty($option3)){ $option3 = 0; }
               $data = array( 

                      "id" =>$value['ProductID'],
                      "parent_id" => $value['ProductID'],
                      "option1" =>$option1,
                      "option2" =>$option2,
                      "option3" =>$option3,
                      "inventory_management"=>"shopify",
                      "price" => $value['SellPrice'], 
                      "inventory_quantity" => $value['Stock'],
                      "presentment_prices"=>array("price"=>array("currency_code"=>"GBP","amount"=>$value['SellPrice']))
                    );
                      array_push($productPayload['variants'],$data);
              }
              }else{
                $option1  = $parentProducts[$i]['Weight'];
                $option2 = $parentProducts[$i]['Height/Length'];
                $option3 = $parentProducts[$i]['Width'];
                if(empty($option1)){ $option1 = 0; }
                if(empty($option2)){ $option2 = 0; }
                if(empty($option3)){ $option3 = 0; }

                $productPayload['variants'][] = array( 

                "id" =>$parentProducts[$i]['ProductID'],
                "parent_id" => $parentProducts[$i]['ProductID'],
                "option1" =>$option1,
                "option2" =>$option2,
                "option3" =>$option3,
                "inventory_management"=>"shopify",
                "price" =>$parentProducts[$i]['SellPrice'],
                "inventory_quantity" => $parentProducts[$i]['Stock'],
                "presentment_prices"=>array("price"=>array("currency_code"=>"GBP","amount"=>$parentProducts[$i]['SellPrice']))
              );
              }
              
             $this->productAddShopify($productPayload,$parentProducts[$i]['ProductID']);
             sleep(2);
            }
              }
               /*====== Product add code end =======*/
            }
          return back()->with('success','Import successfully!');
          }else{
            return back()->with('error','Use correct header in csv file');
          }
            /*check header name in csv end*/
          }
        }
    }

     public function productAddShopify($productPayload,$productID){

         $todayDate = date('Y-m-d H:i:s');
         $this->error_log($todayDate);
         $text = "Sheet Array => ";
         $this->error_log($text);
         $this->error_log($productPayload);
         $text = "Sheet product id => ";
         $this->error_log($text);
         $this->error_log($productID);

      $checkProductId = Product::where('product_id',$productID)->get();
      $checkProductId = $checkProductId->toArray();

      $text = "product id exists into database => ";
      $this->error_log($text);
      $this->error_log($checkProductId);

      if(empty($checkProductId)){

        $productCreateRS = app('App\Http\Controllers\Api\ApiController')->executeShopifyCurl('products.json','POST',json_encode(array('product'=>$productPayload))); 

        if(!empty($productCreateRS->product)){

        foreach ($productPayload['variants'] as $key => $value) {

          $product_id = $productPayload['variants'][$key]['id'];
          $product_parent_id = $productPayload['variants'][$key]['parent_id'];
          $shopify_product_id = $productCreateRS->product->id;
          $shopify_product_variant_id = $productCreateRS->product->variants[$key]->id;
          $product_name = $productPayload['title'];
          $quantity = $productPayload['variants'][$key]['inventory_quantity'];
          //insert into database
          $Product = Product::create([  
                'product_id' => $product_id,
                'product_parent_id' => $product_parent_id,
                'shopify_product_id' => $shopify_product_id,
                'shopify_product_variant_id' => $shopify_product_variant_id,
                'product_name' => $product_name ,
                'quantity'    =>$quantity
            ]);
          $Product->save();
        }  
        $product_id =  $productCreateRS->product->id.'<br>';

        echo $product_id;
        $text = "Shopify product Id =>";
        $this->error_log($text);
       $this->error_log($product_id);

      }else{
        print_r($productCreateRS);

        $text = "Shopify Errors =>";
        $this->error_log($text);
        $this->error_log($productCreateRS);
      }
    }
  }
  public function error_log($data){

    $filepath = public_path().'/shopify_added_product.txt';
    $file = fopen($filepath,"a");
    fwrite($file,print_r($data ,true));
    fclose($file);
  }
  /*===== update product variant option in shopify =========*/
  public function updateShopifyProductVariant($product_variant_id,$weight,$height,$width){
   
    $productVariantId = $product_variant_id;
    $productBody  = array('variant' => array('id'=>$productVariantId,'option1' => $weight,'option2' => $height,'option3' => $width));
    $rs = $this->shopifyapi->updateVariant($productVariantId,$productBody);
    /*print_r($rs);*/
    echo "product updated successfully = ".$productVariantId."<br>";
    echo "update others product"."<br>";
  }
  
  public function updateShopifyRingProductVariant($product_variant_id,$weight,$size){
   
    $productVariantId = $product_variant_id;
    $productBody  = array('variant' => array('id'=>$productVariantId,'option1' => $weight,'option2' => $size));
    $rs = $this->shopifyapi->updateVariant($productVariantId,$productBody);
    print_r($rs);
    echo "product updated successfully = ".$productVariantId."<br>";
    echo "update chain and rings product"."<br>";
  }



   /*====== DB product Delete =======*/
  public function productDelete(){ 
     return view('shopifyProduct.delete'); 
  }
  public function delete(Request $request){
   
    $request->validate([
            'file' => 'required'
    ], [
        'file.required' => 'Please select product csv file'
    ]);
    $file = $request->file('file');
    if($file){
      $extension = $file->getClientOriginalExtension();
      $fileName  = $file->getFilename().'.'.$extension;
      $success = Storage::disk('public')->put($fileName ,  File::get($file));
      $filePath = public_path($fileName);

      if($filePath){
        $csvData=array_map('str_getcsv',file($file));
        $csvHeader = $csvData[0];
        unset($csvData[0]);
        $finalData = array();

        if(trim($csvHeader[0]) == 'ProductID'){
          foreach($csvData as $row){
            array_push($finalData,array_combine($csvHeader, $row));
          }

          foreach ($finalData as $key => $value) {
            $product_id = $value['ProductID'];
            if(!empty(trim($product_id)) || $product_id != 'NULL'){
                $id = Product::where('product_id',$product_id)->get();
                $id = $id->toArray();
                if(!empty($id)){
                  $shopify_product_id = $id[0]['shopify_product_id'];
                  Product::destroy($id[0]['id']);
                  $productDeleteRS = app('App\Http\Controllers\Api\ApiController')->executeShopifyCurl('products/'.$shopify_product_id.'.json','DELETE'); 
                  echo $shopify_product_id."=>delete sccessfully"; 
                }else{
                  echo $product_id." => product did not Found";
                }
            } 
          }
          return redirect()->back()->with('success', "Product delete successfully");
        }
        else{
          return redirect()->back()->with('error', "Use correct header in csv");
        }
      } else{
        return redirect()->back()->with('error', "Csv file did not upload. Try again later");
      }
    }
    
  }
 
  public function imageUpload($product_id=''){
 
   /* $productPayload = array();
    $image_status = Product::where('image_status','false')->get();
    $image_arr = $image_status->toArray();
  
    if ($image_arr) {
      $img_path=public_path('uploads/DANImages/'.$image_arr[0]['product_id'].'.jpg');
      
      if(is_file($img_path)){

        $image = 'https://apihandt.co.uk/uploads/DANImages/'.$image_arr[0]['product_id'].'.jpg';
        $productPayload['src'] =$image;
        $productPayload['variant_ids'] = array($image_arr[0]['shopify_product_variant_id']);
        $Product_id = $image_arr[0]['shopify_product_id'];
        $productCreateRS = app('App\Http\Controllers\Api\ApiController')->executeShopifyCurl('products/'.$Product_id.'/images.json','POST',json_encode(array('image'=>$productPayload))); 
       }
    }*/
   
    /*if($parentProducts[$i]['ParentProductID']) {
      $this->imageUpload($parentProducts[$i]['ParentProductID']);
    }else{
        $this->imageUpload($parentProducts[$i]['ProductID']);
    }*/

    $productId = $product_id."_";
    //$productId = "123456789_";
    $mainFolderPath = public_path('uploads/images');
    $directories  = glob("$mainFolderPath/$productId*.{jpg,png,gif,jpeg}", GLOB_BRACE);
    $images     = array();
    $test     = array();
    $productPayload =array();
    if(!empty($directories)){
      foreach ($directories as $image) {
        $imageName  = basename($image);
        $imageName  = explode("_", $imageName);
        
        $images_name = 'https://apihandt.co.uk/uploads/images/'.$productId.$imageName[1];
        array_push($images, $images_name);
      }
      foreach ($images as $key => $value) {
        $test['src'] = $value;
       array_push($productPayload, $test);
      }
    return $productPayload;
    }else{
     return $productPayload;
    }
  
  }

}
