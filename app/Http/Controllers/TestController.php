<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Shopify\ProductAPI;
use App\Services\DanJewellers\OrderAPI;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class TestController extends Controller
{  
 
	public function __construct()
    {
        $this->shopifyapi = ProductAPI::init();
        $this->danJjewellersapi = OrderAPI::init();

    }

    public function listShopifyProduct()
    {
        $rs = $this->shopifyapi->getProducts();
        print_r($rs);       

    }

    public function updateShopifyProduct()
    {
        $productsId = 4317016031277;
        $productBody  = array('product' => array('id'=>$productsId,'title' => 'Updated via APi'));
        $rs = $this->shopifyapi->updateProduct($productsId,$productBody);
        print_r($rs);
    }

    function createTestOrderOnSupplier(Request $request){
         $order = (array) $request->json()->all();
         $ordero = array('OrderXml' => '<Order>
                                            <OrderHeader>
                                            <PurchaseOrderNumber>OURREF123</PurchaseOrderNumber>
                                            <Notes>Placed via API</Notes>
                                            <CustomerName>Mr. Andrew Smith</CustomerName>
                                            <DeliveryAddress>1 Any Street, Any Town, County AA1
                                            1AA</DeliveryAddress>
                                            </OrderHeader>
                                            <OrderLines>
                                            <OrderLine ProductID="1192" Quantity="3"/>
                                            </OrderLines>
                                        </Order>'
                            );
         $rs = $this->danJjewellersapi->createOrder($order);
         return response()->json( $rs);
        
    }


    public function createProductOnShopify(Request $request)
    {
   
      // $data = file_get_contents("php://input");    

        $json = (array) $request->json()->all();

        $filepath = public_path().'/create_product.txt';
        $file = fopen($filepath,"a");
         fwrite($file,print_r($json ,true));
        fclose($file);
          
        $productBody  = array('product' => array('title' => $json['body_html'],'body_html' => $json['body_html'],'vendor' => $json['vendor'],'product_type' => $json['product_type'],'tags' => $json['tags']));
        $rs = $this->shopifyapi->createroduct($productBody);  
       return response()->json($rs);
    }

    public function updateProductOnShopify(Request $request)
    {  
        $json = (array) $request->json()->all();
        $productBody  = array('product' => $json);
        $rs = $this->shopifyapi->updateProduct($json['id'],$productBody);
        return response()->json($rs);
    }

    public function test()
    {   
        $testValue = "Working";
        return response()->json($testValue);
    }
    

}
