<?php
 
namespace App\Services\Shopify;

use GuzzleHttp;

class ProductAPI
{
    private $apiKey;
    private $apiSecret;
    private $client;


    public function __construct($config, $client = null)
    {  
        $this->apiKey = $config['shopify_api_key'];
        $this->apiSecret = $config['shopify_api_secret'];
        $this->client = $client ?: new GuzzleHttp\Client([
            'base_uri' => $config['shopify_api_base_uri'],
            'headers' => [
                 'Authorization' => 'Basic '.base64_encode($this->apiKey.':'.$this->apiSecret)
            ]

        ]);
    }
   
   /*Init API*/
   public static function init(){
        return new ProductAPI([
            'shopify_api_key' => config('thirdparty.shopify_api_key'),
            'shopify_api_secret' => config('thirdparty.shopify_api_secret'),
            'shopify_api_base_uri' => config('thirdparty.shopify_api_base_uri'),
        ]);
   }
   

   /*List Product*/
   public function getProducts(){    
        try {
            $response = $this->client->get('/admin/api/2019-10/products.json');    
            return $rs = json_decode($response->getBody());
        }catch (\Exception $e) {
            return false;
        }
   }

   /*Update Product*/
   public function updateProduct($id,$data){    
        try {
            $response = $this->client->put('/admin/api/2019-10/products/'.$id.'.json', ['json'=>$data]);    
            return $rs = json_decode($response->getBody());
        }catch (\Exception $e) {
            return false;
        }
   }  

    /*Update Product*/
   public function createroduct($data){    
        try {
            $response = $this->client->post('/admin/api/2019-10/products.json', ['json'=>$data]);    
            return $rs = json_decode($response->getBody());
        }catch (\Exception $e) {
            //print_r( $e->getMessage());
            return false;
        }
   }  

   public function updateVariant($id,$data){    
        try {
            $response = $this->client->put('/admin/api/2019-10/variants/'.$id.'.json', ['json'=>$data]);    
            return $rs = json_decode($response->getBody());
        }catch (\Exception $e) {
            return false;
        }
   } 

   public function updateVariantQuantity($id,$data){    
        try {
            $response = $this->client->put('/admin/variants/'.$id.'.json', ['json'=>$data]);    
            return $rs = json_decode($response->getBody());
        }catch (\Exception $e) {
            return false;
        }
   } 

   public function createCollection($data){
       try {
            $response = $this->client->post('/admin/custom_collections.json', ['json'=>$data]);    
            return $rs = json_decode($response->getBody());
        }catch (\Exception $e) {
            return false;
        }
   } 
   
}
