<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     protected $fillable = [
        'product_id',
        'product_parent_id',
        'shopify_product_id',
        'shopify_product_variant_id',
        'product_name',
        'quantity',
        'price',

     ];
     protected $table = 'products';
}
