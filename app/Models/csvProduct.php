<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 
class csvProduct extends Model
{
     protected $fillable = [
        'ProductID',
        'ParentProductID',
        'ProductName',
        'OptionName',
        'ShortDescription',
        'LongDescription',
        'Weight',
        'Height',
        'Width',
        'Depth',
        'CostPrice',
        'SellPrice',
        'Category',

     ];
     protected $table = 'csv_products';
}
