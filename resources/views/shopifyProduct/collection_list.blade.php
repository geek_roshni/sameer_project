@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Product Collection List</div>
                <div class="card-body">
                    <table>
                        <tr>
                            <th>Collection ID</th>
                            <th>Collection Title</th>
                        </tr>
                        @foreach($collections as $collection)
                            <tr>
                                <td>{{ $collection->collection_id}}</td>
                                <td>{{ $collection->title}}</td>
                            </tr>
                        @endforeach
                       
                    </table>
                    <div class="pagination">
                      {{ $collections->links() }}
                    </div>
                    <div class="return_page">
                        <p>Go To:</p>
                        <a href="{{route ('createCollection')}}">product Form</a>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
