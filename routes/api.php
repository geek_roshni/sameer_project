<?php
 
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
  
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/createProductOnShopify', 'TestController@createProductOnShopify');
Route::post('/updateProductOnShopify', 'TestController@updateProductOnShopify');
Route::post('/createTestOrderOnSupplier', 'TestController@createTestOrderOnSupplier');

Route::post('/updateQuantity', 'SupplierController@updateProductQuantity');


Route::any('/productUpdateCallback', 'Webhooks\DanJewellersController@handleStockUpdate');
Route::any('/shopifyOrderCallback', 'Webhooks\ShopifyController@handleNewOrder');